const tailwindcss = require(`tailwindcss`)
const postcssNested = require('postcss-nested')

module.exports = {
	plugins: [
		tailwindcss(`./tailwind.config.js`),
		postcssNested(),
		require(`autoprefixer`),
		require(`cssnano`)({
			preset: `default`
		})
	]
}
