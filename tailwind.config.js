// See https://tailwindcss.com/docs/configuration for details

module.exports = {
	theme: {
		extend: {
			fontFamily: {
				sans: ['Inter', 'sans-serif'],
				serif: ['Roboto Slab', 'serif'],
				mono: ['SFMono-Regular', 'Menlo']
			},
			colors: {
				'eerie-black': '#1B1B1B',
				sapphire: '#255F85',
				flame: '#D95029',
				'flame-dark': '#A03B1E',
				mustard: '#FFC857',
				offwhite: '#F0F0F0'
			},
			inset: {
				'1/8': '12%',
				'1/4': '24%',
				'1/2': '50%',
				'3/4': '75%'
			},
			maxHeight: {
				xs: '20rem'
			},
			maxWidth: {
				'14': '14rem',
				'20': '20rem',
				'960': '960px'
			}
		}
	}
}
