const path = require('path')
const { createFilePath } = require('gatsby-source-filesystem')

exports.createPages = async ({ actions, graphql, reporter }) => {
	const { createPage } = actions
	const pagesTemplate = path.resolve(`src/templates/pages.js`)
	const result = await graphql(`
		{
			allFile(filter: { sourceInstanceName: { eq: "pages" } }) {
				edges {
					node {
						sourceInstanceName
						childMdx {
							body
							frontmatter {
								path
								title
							}
							id
							excerpt
						}
					}
				}
			}
		}
	`)
	if (result.errors) {
		reporter.panicOnBuild(`Error while running GraphQL query.`)
		return
	}

	result.data.allFile.edges.forEach(({ node }) => {
		createPage({
			path: node.childMdx.frontmatter.path,
			component: pagesTemplate,
			context: { id: node.childMdx.id }
		})
	})
}
