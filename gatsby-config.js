const path = require('path')
const breaks = require('remark-breaks')

module.exports = {
	siteMetadata: {
		title: `Willimantic Public Library`,
		description: `The Willimantic Public Library is the main library for Windham and Willimantic, CT.  All Connecticut
		residents are welcome to check out our materials.`,
		author: `Dan Paquette`
	},
	plugins: [
		`gatsby-plugin-react-helmet`,
		`gatsby-plugin-styled-components`,
		{
			resolve: `gatsby-plugin-manifest`,
			options: {
				name: `willimantic-public-library`,
				short_name: `wpl`,
				start_url: `/`,
				background_color: `#ffffff`,
				theme_color: `#4dc0b5`,
				display: `minimal-ui`,
				icon: `src/assets/images/wpl-favicon.png`
			}
		},
		{
			resolve: `gatsby-plugin-alias-imports`,
			options: {
				alias: {
					'@': path.resolve(__dirname, 'src')
				},
				extensions: []
			}
		},
		// images
		{
			resolve: `gatsby-source-filesystem`,
			options: {
				name: `images`,
				path: path.join(__dirname, `src`, `assets`, `images`)
			}
		},
		`gatsby-transformer-sharp`,
		`gatsby-plugin-sharp`,
		// markdown pages
		{
			resolve: `gatsby-source-filesystem`,
			options: {
				name: `pages`,
				path: `${__dirname}/content/pages`
			}
		},
		// events
		{
			resolve: `gatsby-source-filesystem`,
			options: {
				name: `events`,
				path: `${__dirname}/content/events/`
			}
		},
		{
			resolve: `gatsby-plugin-mdx`,
			options: {
				extensions: [`.mdx`, `.md`],
				remarkPlugins: [breaks]
			}
		},
		`gatsby-plugin-offline`,
		`gatsby-plugin-postcss`,
		{
			resolve: `gatsby-plugin-purgecss`,
			options: {
				tailwind: true,
				purgeOnly: [`src/css/main.css`]
			}
		}
	]
}
