import React from 'react'
import { graphql } from 'gatsby'

import Layout from '@/layouts/default'
import { MDXRenderer } from 'gatsby-plugin-mdx'

export default function Template({ data, ...otherStuff }) {
	console.table(data)
	const {
		mdx: { body, frontmatter }
	} = data
	// const { markdownRemark } = data // data.markdownRemark holds your post data
	// const { frontmatter, html } = markdownRemark
	// const htmlWithBreaks = html.replace(/\n/g, '<br/>')

	return (
		<Layout title={frontmatter.title} mainClass="content">
			{/* <div
				className="content"
				dangerouslySetInnerHTML={{ __html: html }}
			/> */}
			<MDXRenderer>{body}</MDXRenderer>
		</Layout>
	)
}

export const pageQuery = graphql`
	query($path: String!) {
		mdx(frontmatter: { path: { eq: $path } }) {
			body
			excerpt(pruneLength: 160)
			frontmatter {
				title
				path
			}
		}
	}
`
