import React from 'react'
import PropTypes from 'prop-types'

import 'typeface-roboto-slab'
import 'typeface-inter'

import '@/css/main.css'

import Footer from '@/components/footer'

function Layout({ children }) {
	return (
		<>
			{children}
			<Footer />
		</>
	)
}

Layout.propTypes = {
	children: PropTypes.node.isRequired
}

export default Layout
