import Footer from '@/components/footer'
import Navbar from '@/components/navbar'
import SEO from '@/components/seo'
import { graphql, useStaticQuery } from 'gatsby'
import Img from 'gatsby-image'
import PropTypes from 'prop-types'
import React from 'react'
import './default.css'

function Layout({ title, showHero, mainClass, children }) {
	const data = useStaticQuery(graphql`
		query {
			file(relativePath: { eq: "library-looking.jpg" }) {
				childImageSharp {
					fluid {
						...GatsbyImageSharpFluid_withWebp
					}
				}
			}
		}
	`)

	return (
		<>
			<SEO title={title} />
			<Navbar />
			{showHero && (
				<section className="relative">
					<Img
						fluid={data.file.childImageSharp.fluid}
						alt="man looking in library"
						className="hero-image max-h-xs"
					/>
					<h1 className="page-title text-center w-full text-white text-6xl">
						{title}
					</h1>
				</section>
			)}

			<main className={`w-full max-w-960 mx-auto p-8 ${mainClass}`}>
				{children}
			</main>

			<Footer />
		</>
	)
}

Layout.propTypes = {
	children: PropTypes.node.isRequired,
	title: PropTypes.string.isRequired,
	showHero: PropTypes.bool
}

Layout.defaultProps = {
	showHero: true,
	mainClass: ''
}

export default Layout
