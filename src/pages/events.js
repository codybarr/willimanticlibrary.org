import React from 'react'

import Layout from '@/layouts/default'
import dogIllustration from '@/assets/images/dog-illustration.svg'

function EventPage() {
	return (
		<Layout title="Events" mainClass="content">
			<h2>Here are some of our upcoming events</h2>
			<p>
				Consequat culpa reprehenderit amet exercitation do duis. Est
				aliqua ut elit ullamco et velit cillum aliqua dolor et commodo
				adipisicing. Excepteur ullamco ad cupidatat consequat
				reprehenderit qui incididunt eu deserunt irure aliqua fugiat
				fugiat. Laborum Lorem culpa eu tempor aliquip excepteur ipsum
				quis mollit. Cillum veniam reprehenderit Lorem labore cupidatat
				non esse ipsum eiusmod cillum do incididunt non. Mollit commodo
				aliqua reprehenderit in.
			</p>
		</Layout>
	)
}

export default EventPage
