import React from 'react'

import Layout from '@/layouts/front'
import SEO from '@/components/seo'

import FullHero from '@/components/front/full-hero'
import Navbar from '@/components/navbar'
import OpenClosed from '@/components/front/open-closed'
import JumpLinks from '@/components/front/jump-links'

function IndexPage() {
	return (
		<Layout>
			<SEO
				keywords={[`library`, `willimantic`, `windham`, `public`]}
				isFront={true}
				title="Home"
			/>
			<FullHero>
				<Navbar />
				<section className="Slogan">
					<h1 className="text-gray-100 text-5xl px-4 lg:px-0">
						Bringing books to life!
					</h1>
				</section>
				<OpenClosed />
				<JumpLinks className="hidden lg:block" />
			</FullHero>
			<JumpLinks className="lg:hidden" />
			{/* <section className="sapphire-bg text-white p-12 content">
				<h2 className="text-4xl">Important Library News</h2>
				<h3 className="title text-2xl">Hours</h3>
				<p>
					Due to temporary staffing issues, summer hours will be
					extended until further notice. We apologize for the
					inconvenience
				</p>
				<h3 className="text-2xl">Annual Report</h3>
				<p>
					Read our 2019 Annual Report to find out about some of the
					great library happenings from the last year!
				</p>
			</section> */}

			<main className="p-12 content">
				<h2>About the Library</h2>
				<p>
					Nostrud proident cupidatat irure aute velit dolore qui anim
					id aute occaecat aliquip duis. In adipisicing esse proident
					dolor officia excepteur mollit et laboris magna qui ad. Est
					do culpa fugiat anim esse ut ut nisi veniam consequat do
					aliquip id. Aliqua qui aliqua qui culpa reprehenderit fugiat
					aliqua aute sit do incididunt veniam ut elit. Reprehenderit
					irure ea consectetur culpa ut voluptate ea deserunt laboris.
					Reprehenderit fugiat commodo laborum amet veniam non veniam
					esse ea elit non ipsum. Do exercitation enim ex Lorem.
				</p>
			</main>
		</Layout>
	)
}

export default IndexPage
