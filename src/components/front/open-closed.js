import React from 'react'

import dayjs from 'dayjs'
import isBetween from 'dayjs/plugin/isBetween'
import { Link } from 'gatsby'

import bookOpen from '@/assets/images/book-open.svg'

const hours = [
	{
		openToday: false
	},
	{
		open: 9,
		closed: 18,
		openToday: true
	},
	{
		open: 12,
		closed: 20,
		openToday: true
	},
	{
		open: 9,
		closed: 18,
		openToday: true
	},
	{
		open: 12,
		closed: 20,
		openToday: true
	},
	{
		open: 9,
		closed: 17,
		openToday: true
	},
	{
		openToday: false
	}
]

function OpenClosedToday() {
	const today = dayjs()
	const hoursToday = hours[today.day()]

	const openToday = hoursToday.openToday
	const openOrClosed = openToday ? 'open' : 'closed'

	const hourOpen = openToday ? dayjs().hour(hoursToday.open) : ''
	const hourClosed = openToday ? dayjs().hour(hoursToday.closed) : ''

	return (
		<aside className="OpenClosed flame-bg text-white w-full">
			<div className="wrapper relative overflow-hidden p-8 lg:p-12">
				<img
					alt="book background image"
					className="text-white w-20 absolute bottom-1/2 right-1/8 rotate-right opacity-25"
					src={bookOpen}
				/>
				<img
					alt="book background image"
					className="text-white w-20 absolute bottom-1/8 right-3/4 rotate-left opacity-25"
					src={bookOpen}
				/>
				<h3 className="text-lg md:text-2xl relative font-sans">
					The library is {openOrClosed} today
				</h3>
				{openToday && (
					<h2 className="text-2xl md:text-4xl relative mt-2 whitespace-no-wrap">
						{hourOpen.format('h:00 A')} –{' '}
						{hourClosed.format('h:00 A')}
					</h2>
				)}

				<Link
					to="/hours"
					className="relative inline-block text-md md:text-lg mt-2 text-white hover:text-white hover:underline"
				>
					View our full hours <span className="arrow">⟶</span>
				</Link>
				{/* {openToday && (
					<em className="relative block text-xs text-lighter">
						* We are closed on all legal holidays.
					</em>
				)} */}
			</div>
		</aside>
	)
}

export default OpenClosedToday
