import React from 'react'
import { Link } from 'gatsby'

import './jump-links.css'

function JumpLinks({ className }) {
	return (
		<aside className={className + ' JumpLinks bg-offwhite p-12'}>
			<article>
				<h2 className="title-accent">
					<Link to="/services/getting-a-library-card/">
						Library Cards
					</Link>
				</h2>
				<p>
					Et aliqua sint elit adipisicing veniam consectetur aliqua et
					eu irure esse elit consectetur duis.
				</p>
				<Link to="/services/getting-a-library-card/">
					Read More <span className="arrow">⟶</span>
				</Link>
			</article>
			<article>
				<h2 className="title-accent">
					<Link to="/about/library-card/">Computers &amp; Wi-Fi</Link>
				</h2>
				<p>
					Et aliqua sint elit adipisicing veniam consectetur aliqua et
					eu irure esse elit consectetur duis.
				</p>
				<Link to="/about/library-card/">
					Read More <span className="arrow">⟶</span>
				</Link>
			</article>
		</aside>
	)
}

export default JumpLinks
