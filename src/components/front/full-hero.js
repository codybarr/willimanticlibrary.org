import PropTypes from 'prop-types'
import React from 'react'

import './full-hero.css'

function FullHero({ children }) {
	return <div className="FullHero">{children}</div>
}

FullHero.propTypes = {
	children: PropTypes.node.isRequired
}

export default FullHero
