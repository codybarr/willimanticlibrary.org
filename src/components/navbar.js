import { Link } from 'gatsby'
import React, { useState } from 'react'

import Logo from '@/components/logo'
import { Instagram } from 'react-feather'

import './navbar.css'

function navbar() {
	const [isOpen, setOpen] = useState(false)

	const openBurger = (
		<div
			onClick={() => setOpen(false)}
			className="burger-wrapper flex lg:hidden"
			data-ix="burger-click"
		>
			<div className="line-1"></div>
			<div className="line-2"></div>
			<div className="line-3"></div>
		</div>
	)

	const closedBurger = (
		<div
			onClick={() => setOpen(true)}
			className="burger-wrapper closed flex lg:hidden"
			data-ix="burger-click"
		>
			<div className="line-1"></div>
			<div className="line-2"></div>
			<div className="line-3"></div>
		</div>
	)

	return (
		<header className="Header w-full bg-white px-4 py-6">
			<div className="w-full flex justify-between items-center">
				<Link to="/" className="text-eerie-black hover:text-flame-dark">
					<Logo className="h-12 md:h-16 fill-current" />
				</Link>
				{isOpen ? openBurger : closedBurger}
			</div>
			<nav
				className={`${
					isOpen ? 'isOpen' : ''
				} bg-white p-4 pb-6 lg:p-0 uppercase tracking-wide font-bold w-full block flex-grow lg:flex lg:flex-initial lg:w-auto items-center lg:mt-0 z-10`}
			>
				<ul>
					<li>
						<Link to="/services/" activeClassName="active">
							Services
						</Link>
					</li>
					<li>
						<Link to="/about/" activeClassName="active">
							About
						</Link>
					</li>
					<li>
						<Link to="/events/" activeClassName="active">
							Events
						</Link>
					</li>
					<li>
						<Link to="/media/" activeClassName="active">
							Books &amp; Media
						</Link>
					</li>
					<li>
						<div className="social-links lg:ml-8">
							<a
								href="http://instagram.com/willimanticpubliclibrary"
								target="_blank"
								rel="noreferrer"
								className="social-icon"
							>
								<Instagram color="currentColor" />
							</a>
						</div>
					</li>
				</ul>
			</nav>
		</header>
	)
}

export default navbar
