import React from 'react'
import Logo from '@/components/logo'
import { Link } from 'gatsby'
import { Instagram } from 'react-feather'

import './footer.css'

function footer() {
	return (
		<footer className="lg:flex bg-offwhite mt-8">
			<div className="footer-wrapper mx-auto flex flex-wrap lg:flex-no-wrap justify-start sm:justify-around">
				<section>
					<Logo className="w-64" />
					<address className="mt-6">
						<a
							href="https://goo.gl/maps/rtgRUWzKJytpNTtu5"
							target="_blank"
							rel="noreferrer"
						>
							905 Main Street
							<br />
							Willimantic, CT 06226
						</a>
						<br />
						<a href="tel:+18604653079">860-465-3079</a>
					</address>
					<a
						href="http://instagram.com/willimanticpubliclibrary"
						target="_blank"
						rel="noreferrer"
						className="social-icon mt-6 bg-gray-400"
					>
						<Instagram color="currentColor" />
					</a>
				</section>
				<section>
					<h2>Using the Library</h2>
					<ul>
						<li>
							<Link to="/services/getting-a-library-card">
								Getting a Library Card
							</Link>
						</li>
						<li>
							<Link to="/">Loan Periods</Link>
						</li>
						<li>
							<Link to="/">Computers &amp; Wi-Fi</Link>
						</li>
						<li>
							<Link to="/">Museum Passes</Link>
						</li>
						<li>
							<Link to="/">Policies</Link>
						</li>
						<li>
							<Link to="/contact/">Kids</Link>
						</li>
					</ul>
				</section>

				<section>
					<h2>Books, Media, &amp; More</h2>
					<ul>
						<li>
							<Link to="/">Catalog</Link>
						</li>
						<li>
							<Link to="/">eBooks &amp; Audiobooks</Link>
						</li>
						<li>
							<Link to="/">Databases</Link>
						</li>
						<li>
							<Link to="/">Resources</Link>
						</li>
						<li>
							<Link to="/">Frog Citry Stories</Link>
						</li>
					</ul>
				</section>
				<section>
					<h2>About the Library</h2>
					<ul>
						<li>
							<Link to="/">Support the Library</Link>
						</li>
						<li>
							<Link to="/">Friends of the Library</Link>
						</li>
						<li>
							<Link to="/">Board of Directors</Link>
						</li>
					</ul>
					<h2>
						<Link to="/contact/">Contact Us</Link>
					</h2>
				</section>
			</div>
		</footer>
	)
}

export default footer
