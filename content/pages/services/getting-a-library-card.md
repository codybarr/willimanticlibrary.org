---
title: 'Getting a Library Card'
path: '/services/getting-a-library-card'
---

We issue new cards daily until 15 minutes prior to closing.
Cards are issued for three years and can be renewed at that time.
Your Willimantic Public Library card may be used at all Connecticut public libraries.

## Children and Young Adults

A parent or legal guardian who must show a valid unexpired photo ID (photocopied IDs are not accepted) and a piece of mail that has been received at their residence within the last three months. Mail can be sent to a PO box, however a parent or legal guardian must provide proof of residency to be issued a card. There is a \$1.00 fee to replace a lost card.
