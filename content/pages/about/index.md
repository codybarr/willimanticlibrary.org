---
title: About
path: "/about"
files: "/uploads/Friends_Membership2019.pdf"

---
Willimantic Public Library
905 Main Street
Willimantic, CT 06226

## Hours:

-   Monday: 9-6
-   Tuesday: 12-8
-   Wednesday: 9-6
-   Thursday: 12-8
-   Friday: 9-5
-   Saturday: Closed due to temporary staffing issues.
-   Sunday: Closed

**We are closed on all legal holidays.**

## Library Director

Phone: 860-465-3080
Email: Dan Paquette - dpaquette@biblio.org

## Adult Department

Phone: 860-465-3079
Email: Julia Gavin - jgavin@biblio.org

## Youth Department

Phone: 860-465-3082
Email: Lisa Clymer - lclymer@biblio.org

## Windham Heights Satellite Library

Inside the Learning Center
70 Boston Post Road
Willimantic, CT 06226
Mondays 3:30-5:00