---
title: Books & Media
path: "/media"

---
If you are interested in supporting the Willimantic Public Library by promoting its services and advocating for increased funding in the town budget, please become a member of the Friends of the Library. All are welcome.

![](/uploads/knitting.jpg)

Friends of the Willimantic Public Library contact person:
Nancy Pettitt, 860-456-8179, nancy_p@snet.net

The purpose of the Friends shall be:

* To promote knowledge of functions, resources, services and needs of the Library.
* To foster public support for the necessary development, to increase the facilities and service of Willimantic Public Library; and thus to enrich the cultural opportunities available to the citizens of Willimantic.
* To provide programs and events for the community that are compatible with the Library's policies and objectives.
* To raise funds for programs, books, equipment, and assistance as needed to support the Library's future needs in facilities, equipment or books.